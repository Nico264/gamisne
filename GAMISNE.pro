QT += quick svg xml multimedia quickcontrols2
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    maskedmousearea.cpp \
    gameclient.cpp

HEADERS += \
    maskedmousearea.h \
    gameclient.h

OTHER_FILES += qml/*.qml

qml.files = $$files(qml/*.qml)
RESOURCES += qml

images.files = $$files(images/*)
RESOURCES += images

music.files = $$files(music/*)
RESOURCES += music

general.files = qtquickcontrols2.conf
RESOURCES += general

DISTFILES += \
    qtquickcontrols2.conf
