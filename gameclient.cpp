#include "gameclient.h"
#include <iostream>
using namespace std;

GameClient::GameClient(QObject *parent) : QObject(parent)
{
    in.setDevice(connection);
    in.setVersion(QDataStream::Qt_5_6);
    connect(connection, &QTcpSocket::connected, this, &GameClient::ready);
    connect(connection, &QTcpSocket::readyRead, this, &GameClient::incommingMessage);
    connect(connection, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
            this, &GameClient::connectionError);
}

GameClient::~GameClient() {
    connection->disconnectFromHost();
    connection->deleteLater();
}

void GameClient::signIn(){
    connection->abort();
    connection->connectToHost("localhost", 8080);
}

void GameClient::signOut(){
    connection->disconnectFromHost();
    searchingPlayers.clear();
    emit usernamesChanged();
}

void GameClient::send(int messageType, QVariantList messageContent) {
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);

    out << messageType << messageContent;

    connection->write(block);
    qDebug() << "→" << messageType << messageContent;
}

void GameClient::askPair(int num) {
    QUuid opponentId = searchingPlayers.keys()[num];
    send(AskPair, QVariantList({opponentId}) << m_elephantSet);
    state = WantsPair;
}

void GameClient::finish(int xp) {
    send(Finish, {xp});
}

void GameClient::accept() {
    send(Accept, m_elephantSet);
    state = Playing;
}

void GameClient::refuse() {
    send(Refuse);
    state = Searching;
}

void GameClient::incommingMessage() {
    while (!connection->atEnd()) {
        in.startTransaction();

        int messageType;
        QVariantList payload;
        in >> messageType >> payload;

        if (!in.commitTransaction())
            return;

        qDebug() << "←" << messageType << payload;

        switch (messageType) {
        case NewName:
            if (payload.length() == 2) {
                searchingPlayers[payload[0].toUuid()] = payload[1];
                emit usernamesChanged();
            } else {
                qDebug() << "Malformed message" << payload;
            }
            break;
        case Forget:
            if (payload.length() == 1) {
                searchingPlayers.remove(payload[0].toUuid());
                emit usernamesChanged();
            } else {
                qDebug() << "Malformed message" << payload;
            }
            break;
        case AskPair:
            if (payload.length() == 6) {
                opponentId = payload[0].toUuid();
                m_opponentSet = payload.mid(2, 4);
                state = MayPair;
                emit opponentSetChanged();
                emit pairAsked(payload[1].toString());
            } else {
                qDebug() << "Malformed message" << payload;
            }
            break;
        case Refuse:
            if (state == WantsPair || state == MayPair) {
                opponentId = QUuid();
                state = Searching;
                emit refused();
            } else if (state == Playing) {
                opponentId = QUuid();
                state = Searching;
                signOut();
                emit connectionLost();
            } else {
                qDebug() << "Unconsistant refusal" << payload;
            }
            break;
        case Accept:
            if (payload.length() == 4) {
                if (state == WantsPair) {
                    state = Playing;
                    m_opponentSet = payload.mid(0, 4);
                    emit opponentSetChanged();
                    emit accepted();
                } else {
                    qDebug() << "Unconsistant approval" << payload;
                }
            } else {
                qDebug() << "Malformed message" << payload;
            }
            break;
        case Finish:
            if (payload.length() == 1) {
                if (state == Playing) {
                    state = Finished;
                    emit finished(payload[0].toInt());
                } else {
                    qDebug() << "Unconsistant finish" << payload;
                }
            } else {
                qDebug() << "Malformed message" << payload;
            }
            break;
        default:
            qDebug() << "Unrecognized message…" << messageType << payload;
            break;
        }
    }
}
