#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QUuid>
#include <QDataStream>

enum States {
    Searching,
    WantsPair,
    MayPair,
    Playing,
    Finished
};

enum MessageTypes {
    NewName,
    AskPair,
    Accept,
    Refuse,
    Forget,
    Finish
};

class GameClient : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList usernames READ usernames NOTIFY usernamesChanged)
    Q_PROPERTY(QVariantList opponentSet READ opponentSet NOTIFY opponentSetChanged)
    Q_PROPERTY(QVariantList elephantSet READ elephantSet WRITE setElephantSet NOTIFY elephantSetChanged)
    Q_ENUMS(QAbstractSocket::SocketError)
public:
    explicit GameClient(QObject *parent = nullptr);
    ~GameClient();
    Q_INVOKABLE inline void newName(QString name){ send(NewName, {name}); }
    Q_INVOKABLE void askPair(int);
    Q_INVOKABLE void accept();
    Q_INVOKABLE void refuse();
    Q_INVOKABLE void signIn();
    Q_INVOKABLE void finish(int);

public slots:
    Q_INVOKABLE void signOut();

signals:
    void usernamesChanged();
    void opponentSetChanged();
    void elephantSetChanged();
    void opponentNameChanged();
    void pairAsked(QString opponentName);
    void accepted();
    void refused();
    void connectionLost();
    void finished(int opponentXP);
    void ready();
    void connectionError(QAbstractSocket::SocketError socketError);

private slots:
    void incommingMessage();

private:
    int state;
    QTcpSocket *connection = new QTcpSocket();
    void send(int, QVariantList = {});

    QVariantList m_usernames;
    inline QVariantList usernames(){ return searchingPlayers.values(); }

    QVariantList m_opponentSet;
    inline QVariantList opponentSet(){ return m_opponentSet; }

    QVariantList m_elephantSet;
    inline QVariantList elephantSet(){ return m_elephantSet; }
    inline void setElephantSet(QVariantList set) { m_elephantSet = set; }

    QUuid opponentId;

    QMap<QUuid, QVariant> searchingPlayers;
    QDataStream in;
};

#endif // GAMECLIENT_H
