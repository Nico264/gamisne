#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include "maskedmousearea.h"
#include "gameclient.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationName("GAMISNE");
    app.setOrganizationName("Wiz'Art");
    app.setOrganizationDomain("art.lawiz");
    app.setAttribute(Qt::AA_EnableHighDpiScaling);
    app.setApplicationVersion("1.0");

    qmlRegisterType<MaskedMouseArea>("MaskedMouseArea", 1, 0, "MaskedMouseArea");
    qmlRegisterType<GameClient>("GameClient", 1, 0, "GameClient");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
