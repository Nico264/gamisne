import QtQuick 2.10
import QtQuick.Layouts 1.3

Item {
    id: elephant
    function calculateScale() {
        if (width*1300 < height*1000) {
            container.scale = width/1000
        } else {
            container.scale = height/1300
        }
    }

    onWidthChanged: calculateScale()
    onHeightChanged: calculateScale()

    property bool editing: false
    property bool preparing: false
    property bool mirror: false
    property Item ennemy

    property var parts: [head, ears, body, feet]

    property var set: [head.set, ears.set, body.set, feet.set]

    function fire() {
        var startPoint = sets.getStartPoint(head.set)
        var startX = Math.cos(head.rotation*Math.PI/180)*(mirror ? head.width-startPoint.x : startPoint.x)
        var startY = Math.cos(head.rotation*Math.PI/180)*startPoint.y
        var endX = ennemy.parts[Sets.Head].width*(Math.random()*0.8+0.1)
        var endY = ennemy.parts[Sets.Head].height*(Math.random()*0.6+0.2)
        spell.createObject(parent,
                           {startPoint: parent.mapFromItem(elephant.parts[Sets.Head], startX, startY),
                            endPoint: parent.mapFromItem(ennemy.parts[Sets.Head], endX, endY)})
    }

    function explode() {
        head.explode()
        ears.explode()
        body.explode()
        feet.explode()
    }

    property int xp: head.xp + ears.xp + body.xp + feet.xp

    Item {
        id: container
        anchors.centerIn: parent
        width: 1000
        height: 1300
        scale: 1

        Part{
            id: body
            partType: Sets.Body
            preparing: elephant.preparing
            editing: elephant.editing
            mirror: elephant.mirror
        }
        Part {
            id: feet
            partType: Sets.Feet
            preparing: elephant.preparing
            editing: elephant.editing
            mirror: elephant.mirror
        }
        Part{
            id: ears
            partType: Sets.Ears
            preparing: elephant.preparing
            editing: elephant.editing
            mirror: elephant.mirror

            rotation: head.rotation
        }
        Part{
            id: head
            partType: Sets.Head
            preparing: elephant.preparing
            editing: elephant.editing
            mirror: elephant.mirror

            SequentialAnimation on rotation {
                loops: Animation.Infinite
                RotationAnimation {
                    to: 5
                    duration: Math.max(Math.random(), 0.2)*5000
                }
                RotationAnimation {
                    to: -5
                    duration: Math.max(Math.random(), 0.2)*5000
                }
            }
        }
    }
}
