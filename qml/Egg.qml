import QtQuick 2.10
import QtQuick.Layouts 1.3

Image {
    id: egg

    ParallelAnimation {
        id: tilt
        alwaysRunToEnd: false
        SequentialAnimation {
            RotationAnimation {
                target: egg
                from: 0
                to: -5
                duration: 100
            }
            RotationAnimation {
                target: egg
                to: 5
                duration: 200
            }
            RotationAnimation {
                target: egg
                to: 0
                duration: 100
            }
        }
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            egg.xp += 1
            tilt.restart()
        }
    }

    property int xp: 0
    fillMode: Image.PreserveAspectFit

    source: "qrc:/images/egg.svg"
}
