import QtQuick 2.10
import MaskedMouseArea 1.0
import QtQuick.Particles 2.0
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0

Item{
    id: part
    property int xp: 0
    property int setSelector: 0
    property bool preparing: false
    property bool editing: false

    property int set: sets.unlocked[partType][setSelector]
    property int partType
    property bool mirror: false

    Settings {
        category: "Part"+partType
        property alias setSelector: part.setSelector
    }

    Connections {
        target: sets
        onNewPart: {
            if (part.partType === partType) {
                setSelector = sets.unlocked[partType].length - 1
            }
        }
    }

    function explode() {
        emitter.burst(200)
    }

    width: image.width
    height: image.height

    Image{
        id: image

        mirror: parent.mirror

        source: "qrc:/images/elephant-parts/"+sets.getSetName(set)+"_"+sets.getPartName(partType)+".png"

        Behavior on scale {
            NumberAnimation {
                duration: 50
            }
        }

        scale: mouseArea.pressed ? 0.98 :
               mouseArea.containsMouse ? 1.05 : 1

        MaskedMouseArea{
            id: mouseArea
            anchors.fill: parent

            enabled: editing || preparing

            maskSource: image.source

            onReleased: {
                if (preparing) {
                    xp++
                    settings.totalClicks++
                    settings.currentClicks++
                    settings.clicks[partType]++
                    settings.clicks = settings.clicks
                } else {
                    setSelector = (setSelector+1) % sets.unlocked[partType].length
                }
            }
        }
        ParticleSystem {
            id: particles
            z: 50
        }
        Emitter {
            id: emitter
            system: particles
            anchors.fill: parent
            shape: MaskShape {
                source: image.source
            }

            z: 50

            enabled: false

            size: 40
            sizeVariation: 16

            acceleration: AngleDirection {
                magnitude: 500
                angleVariation: 180
            }
        }
        ImageParticle {
            system: particles
            source: "qrc:/images/sparkler.png"
            z: 50
        }
    }
}
