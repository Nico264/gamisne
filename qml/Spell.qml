import QtQuick 2.10
import QtGraphicalEffects 1.0
import QtQuick.Particles 2.0

Component {
    Item{
        z: 50
        id: spell
        property point startPoint
        property point endPoint
        Rectangle {
            id: rect
            width: 5
            radius: width/2
            height: width
            x: startPoint.x
            y: startPoint.y
            SequentialAnimation {
                running: true
                NumberAnimation {
                    target: rect
                    property: "scale"
                    from: 0
                    to: 5
                    duration: 1000
                }
                ParallelAnimation {
                    PropertyAnimation {
                        target: rect
                        property: "x"
                        to: endPoint.x
                        duration: 500
                    }
                    PropertyAnimation {
                        target: rect
                        property: "y"
                        to: endPoint.y
                        duration: 500
                    }
                }
                ParallelAnimation {
                    ScriptAction {
                        script: {
                            emitter.burst(20)
                            particles.bursted = true
                        }
                    }
                    PropertyAnimation{
                        target: rect
                        property: "scale"
                        to: 0
                        duration: 500
                    }
                }
            }
        }
        Glow {
            id: glow
            anchors.fill: rect
            scale: rect.scale
            radius: 5
            samples: 17
            color: "white"
            source: rect
        }

        ParticleSystem {
            id: particles
            property bool bursted: false
            onEmptyChanged: if (empty && bursted) { spell.destroy() }
        }
        Emitter {
            id: emitter
            system: particles
            anchors.fill: rect

            enabled: false

            size: 24
            sizeVariation: 16

            acceleration: AngleDirection {
                magnitude: 100
                angleVariation: 180
            }
        }
        ImageParticle {
            system: particles
            source: "qrc:/images/sparkler.png"
        }
    }
}
