import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0

Page {
    id: page

    background: Image {
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/images/upgrades/background.png"
    }

    header: RowLayout {
        Label {
            text: "Potions"
            font.pixelSize: Qt.application.font.pixelSize * 2
            padding: 10
            Layout.fillWidth: true
        }
        Label {
            text: settings.currentClicks+" Clicks"
            font.pixelSize: Qt.application.font.pixelSize * 2
            padding: 10
        }
    }

    ListView {
        id: listView
        anchors.fill: parent
        model: upgrades.list

        anchors.margins: 15

        displayMarginBeginning: page.header.height

        delegate: Item {
            height: background.height+row.height-10
            width: background.width
            Image {
                id: background
                source: "qrc:/images/upgrades/cupboard"+Math.floor(Math.random()*2)+".png"
                anchors.top: row.bottom
                anchors.topMargin: -20
                anchors.left: parent.left
                width: (930-46)/930*page.width
            }

            RowLayout {
                id: row
                width: listView.width-30
                anchors.horizontalCenter: background.horizontalCenter
                Label {
                    Layout.leftMargin: 98/930*page.width
                    text: number
                }

                Image {
                    source: "qrc:/images/upgrades/potions/alchemy-potion-animal-youth.png"
                    sourceSize.width: 50
                }

                ColumnLayout {
                    Layout.fillWidth: true
                    Label {
                        Layout.fillWidth: true
                        text: name
                        bottomPadding: 3
                        horizontalAlignment: Text.AlignHCenter
                    }
                    Label {
                        Layout.fillWidth: true
                        text: price+" Clicks"
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                ToolButton {
                    id: info
                    icon.source: "qrc:/images/upgrades/infos.svg"
                    icon.color: "transparent"
                    onClicked: {
                        dialog.title = name
                        dialog.comment = comment
                        dialog.open()
                    }
                }

                ToolButton {
                    id: buy
                    icon.source: "qrc:/images/upgrades/euro.svg"
                    icon.color: "transparent"
                    Layout.rightMargin: 98/930*page.width
                    enabled: price <= settings.currentClicks
                    onClicked: upgrades.unlock(index)
                }
            }
        }
    }

    Dialog {
        id: dialog
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        property string comment
        Label {
            text: dialog.comment
            wrapMode: Text.WordWrap
            anchors.fill: parent
        }
        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)
        topMargin: (page.height-height)/2
        leftMargin: (page.width-width)/2
        modal: true
    }
}
