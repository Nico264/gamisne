import QtQuick 2.10
import QtQuick.Controls 2.3
import Qt.labs.settings 1.0

ApplicationWindow {
    id: window
    visible: true
    title: "GAMISNE"

    property var openedNotifications: []

    Achievements {
        id: achievements
    }

    Upgrades {
        id: upgrades
    }

    Notification {
        id: notification
    }

    Spell {
        id: spell
    }

    Sets {
        id: sets
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        interactive: false

        ElephantPage {
        }

        UpgradesPage {
        }

        AchievementsPage {

        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: "Bataille"
        }
        TabButton {
            text: "Potions"
        }
        TabButton {
            text: "Succès"
        }
    }

    Settings {
        id: settings
        property alias windowWidth: window.width
        property alias windowHeight: window.height
        property alias windowX: window.x
        property alias windowY: window.y

        property string username: "New Born Player"

        property real lootRate: 0.5
        property real directedLootRate: 0

        property bool tutorialSeen: false
        property int totalClicks: 0
        property int currentClicks: 0
        property var clicks: [0, 0, 0, 0]
        property int hatchedEggs: 0
        property int wonOffline: 0
        property int wonOnline: 0
        property int lost: 0

        onWonOnlineChanged: {
            if (wonOnline < 1) return
            achievements.unlock(Achievements.FirstBlood)
            if (wonOnline < 5) return
            achievements.unlock(Achievements.FifthBlood)
            if (wonOnline < 10) return
            achievements.unlock(Achievements.TenthBlood)
            if (wonOnline < 20) return
            achievements.unlock(Achievements.TwentiethBlood)
            if (wonOnline < 50) return
            achievements.unlock(Achievements.FiftiethBlood)
            if (wonOnline < 100) return
            achievements.unlock(Achievements.HundredthBlood)
        }

        onWonOfflineChanged: {
            if (wonOnline < 1) return
            achievements.unlock(Achievements.FirstBot)
            if (wonOnline < 5) return
            achievements.unlock(Achievements.FifthBot)
            if (wonOnline < 10) return
            achievements.unlock(Achievements.TenthBot)
            if (wonOnline < 20) return
            achievements.unlock(Achievements.TwentiethBot)
            if (wonOnline < 50) return
            achievements.unlock(Achievements.FiftiethBot)
            if (wonOnline < 100) return
            achievements.unlock(Achievements.HundredthBot)
        }
    }
}
