import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import GameClient 1.0
import QtMultimedia 5.9
import Qt.labs.folderlistmodel 2.2

Page {
    id: page

    property int preparationTime: 4
    property real fightTime: 5
    property bool won: elephant.xp > ennemy.xp

    property bool onlinePlay: false

    function firstList(string) {
        var lists = {
                    "WIZ": "#ffccff",
                    "CLINT": "brown",
                    "ATLAS": "white",
                    "LORD": "#800000",
                    "STORM": "purple",
                    "JACK": "orange",
                    "IMAG": "chartreuse"
                }

        var i = 0
        var string3
        var string4
        var string5
        while (i<string.length) {
            string3 = string.substr(i, 3).toUpperCase()
            if (string3 in lists) {
                return lists[string3]
            }
            string4 = string.substr(i, 4).toUpperCase()
            if (string4 in lists) {
                return lists[string4]
            }
            string5 = string.substr(i, 5).toUpperCase()
            if (string5 in lists) {
                return lists[string5]
            }

            i++
        }
        return "yellow"
    }

    function startOnlineMatch() {
        ennemy.set = gameClient.opponentSet
        page.onlinePlay = true
        elephant.parts.forEach(function(part){
            part.xp = 0
        })
        page.state = "online_prepare"
    }

    background: Image {
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/images/battle_background.png"

    }

    header: RowLayout {
            Label {
                text: "Champ de bataille"
                elide: Label.ElideRight
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
                font.pixelSize: Qt.application.font.pixelSize * 2
                padding: 10
            }
            ToolButton {
                icon.source: "qrc:/images/wifi.png"
                onClicked: connector.open()
                visible: page.state == "egg" && !tutorial.inProgress
            }
            ToolButton {
                icon.source: "qrc:/images/edit.png"
                onClicked: page.state = (page.state == "edit") ? "egg" : "edit"
                visible: (page.state == "egg" || page.state == "edit") && !tutorial.inProgress
            }
            ToolButton {
                text: "?"
                onClicked: aboutDialog.open()
                visible: (page.state == "egg" || page.state == "edit") && !tutorial.inProgress
            }
        }

    About {
        id: aboutDialog
    }

    Egg {
        id: egg
        anchors.fill: parent
        onXpChanged: {
            if (xp >= 10) {
                achievements.unlock(Achievements.Hatched)
                if (!tutorial.inProgress) {
                    page.state = "prepare"
                } else {
                    tutorial.egg_blown()
                    page.state = "edit"
                }
            }
        }
        visible: false
    }

    Elephant {
        id: elephant
        anchors{
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
        ennemy: ennemy
        onSetChanged: {
            gameClient.elephantSet = set
            if (set[0] === set[1] && set[0] === set[2] && set[0] === set[3]) {
                achievements.unlock(Achievements.CompleteSet)
                achievements.unlock(Achievements.CompleteSet+1+set[0])
            }
        }
    }

    Elephant {
        id: ennemy
        anchors{
            left: parent.right
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
        mirror: true
        ennemy: elephant
        set: Math.floor(Math.random() * sets.setsList.count)
        onSetChanged: {
            if (typeof set == "number") {
                parts.forEach(function(part){
                    part.set = set
                })
            } else {
                parts.forEach(function(part, index){
                    part.set = set[index]
                })
            }
        }
    }

    Timer {
        id: timer
        repeat: true
        interval: 1000
        property int length
        property var onTick: function () {}
        property var onFinished: function () {}
        onTriggered: {
            if (length == 0) {
                onFinished()
            } else {
                onTick()
                length -= 1
            }
        }

        running: false
        triggeredOnStart: false
    }

    Audio {
        id: audioPlayer
        audioRole: Audio.GameRole
        loops: Audio.Infinite
        onError: console.log(errorString)
    }

    FolderListModel {
        id: audioFolder
        folder: "qrc:/music"
    }

    Dialog {
        id: endDialog
        topMargin: (parent.height - height)/2
        leftMargin: (parent.width - width)/2
        title: won ? "Gagné!" : "Perdu…"

        Label {
            id: troll
            wrapMode: Text.WordWrap
            property var trollWon: [
                "Mais vous avez perdu LE JEU…",
                "Don't get cocky",
                "WANNA COOKIE?",
                "D, la réponse D",
                "Votez Wiz",
                "Les ravioles, c'est bon !",
                "C'est L A V A N D E !!!"
            ]
            property var trollLost: [
                "THE GAME",
                "Au moins vous n'avez pas perdu LE JEU…",
                "D, la réponse D",
                "Try again ;-)",
                "Essaie encore !",
                "Don't worry it happens to everyone",
                "Votez Wiz",
                "Better use the Wiz set next time",
                "42% des voix :'("
            ]

            function update() {
                var num
                if (won) {
                    num = Math.floor(Math.random()*trollWon.length)
                    text = trollWon[num]
                } else {
                    num = Math.floor(Math.random()*trollLost.length)
                    text = trollLost[num]
                }
            }
            anchors.fill: parent
        }

        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnReleaseOutside

        onAboutToShow: troll.update()

        onOpened: {
            var audioFileName = audioFolder.get(Math.floor(Math.random()*audioFolder.count), "fileName")
            audioPlayer.source = "qrc:/music/"+audioFileName
            audioPlayer.play()
        }

        onClosed: {
            audioPlayer.stop()
            page.state = "egg"
            if (tutorial.inProgress) {
                tutorial.explain()
            }
        }
    }

    GameClient {
        id: gameClient
        onPairAsked: {
            conDialog.opponentName = opponentName
            conDialog.open()
        }
        elephantSet: elephant.set
        onAccepted: {
            startOnlineMatch()
            connector.close()
            waitForResponse.close()
        }
        onReady: {
            newName(username.text)
        }

        onFinished: {
            ennemy.xp = opponentXP
        }
        onRefused: {
            if (conDialog.opened) {
                conDialog.close()
            } else {
                waitForResponse.close()
            }
            notification.createObject(window,
                                      {titleText: "Refus ou perte de connection",
                                       displayText: "Essayez un autre",
                                       displayTime: 2000})
        }
        onConnectionLost: {
            page.state = "egg"
            notification.createObject(window,
                                      {titleText: "Connection perdue",
                                       displayText: "Jeu annulé…",
                                       displayTime: 2000})
        }
        onConnectionError: {
            if (socketError === 0 || socketError === 2 || socketError ===7){
                notification.createObject(window,
                                          {titleText: "Erreur de connection",
                                           displayText: "Essayez de vous reconnecter",
                                           displayTime: 2000})
                onlinePlay = false
                connector.close()
                gameClient.signOut()
                page.state = "egg"
            }
        }
    }

    Dialog {
        id: connector
        modal: true

        title: "Jeu en ligne"
        topMargin: (parent.height - height)/2
        leftMargin: (parent.width - width)/2
        height: 3*page.height/4
        width: 3*page.width/4
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        ColumnLayout {
            anchors.fill: parent
            TextField {
                id: username
                text: settings.username
                onTextChanged: {
                    if (connector.opened) {
                        gameClient.newName(text)
                        settings.username = text
                    }
                }
                Layout.fillWidth: true
                Layout.fillHeight: false
                selectByMouse: true
            }

            Label {
                text: "On dirait que personne d'autre n'est connecté au serveur…"
                visible: !listView.visible
                wrapMode: Label.WordWrap
                Layout.fillWidth: true
                Layout.fillHeight: false
            }

            Image {
                source: "qrc:/images/field.jpg"
                Layout.fillHeight: true
                Layout.fillWidth: true
                visible: !listView.visible
            }

            ListView {
                id: listView
                model: gameClient.usernames
                Layout.fillHeight: true
                Layout.fillWidth: true
                focus: true
                visible: count > 0
                delegate: Label {
                    text: modelData
                    anchors.left: parent.left
                    anchors.right: parent.right

                    wrapMode: Label.WordWrap

                    color: firstList(text)

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            gameClient.askPair(index)
                            waitForResponse.open()
                        }
                    }
                }
            }
        }

        onAboutToShow: {
            gameClient.signIn()
        }

        onClosed: {
            if (!onlinePlay) {
                gameClient.signOut()
            }
        }
    }

    Dialog {
        id: conDialog
        modal: true
        standardButtons: Dialog.Ok | Dialog.Cancel
        title: "Nouveau jeu"
        topMargin: (parent.height - height)/2
        leftMargin: (parent.width - width)/2
        property string opponentName

        closePolicy: Popup.NoAutoClose

        Label {
            text: conDialog.opponentName+" veut se battre contre vous !"
            wrapMode: Text.WordWrap
            anchors.fill: parent
        }

        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)

        onAccepted: {
            gameClient.accept()
            startOnlineMatch()
            connector.close()
        }
        onRejected: gameClient.refuse()
    }

    Dialog {
        id: waitForResponse
        title: qsTr("En attente de réponse")
        topMargin: (parent.height - height)/2
        leftMargin: (parent.width - width)/2
        modal: true
        standardButtons: Dialog.Cancel
        closePolicy: Popup.NoAutoClose

        Label {
            text: "Votre adversaire a été notifié de votre demande, nous attendons sa réponse"
            wrapMode: Text.WordWrap
            anchors.fill: parent
        }

        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)

        onRejected: {
            connector.close()
        }
    }

    Tutorial {
        id: tutorial
    }

    state: "egg"

    transitions: [
        Transition {
            AnchorAnimation {
                duration: 1000
            }
        },
        Transition {
            to: "egg"
            SequentialAnimation {
                ParallelAnimation {
                    NumberAnimation {
                        target: egg
                        property: "opacity"
                        from: 0
                        to: 1
                        duration: 500
                    }
                    NumberAnimation {
                        targets: [elephant, ennemy]
                        property: "opacity"
                        from: 1
                        duration: 500
                    }
                }
                ScriptAction {
                    script: ennemy.set = Math.floor(Math.random() * sets.setsList.count)
                }
            }
        }
    ]

    states: [
        State {
            name: "edit"
            PropertyChanges {
                target: elephant
                editing: true
            }
        },
        State {
            name: "egg"
            PropertyChanges {
                target: egg
                visible: true
                xp: 0
            }
            PropertyChanges {
                target: elephant
                visible: false
                opacity: 0
            }
            PropertyChanges {
                target: ennemy
                opacity: 0
            }
            StateChangeScript {
                script: {
                    if (!settings.tutorialSeen) {
                        tutorial.start()
                    }

                    elephant.parts.forEach(function(part) {
                        part.xp = 0
                    })
                    ennemy.xp = Math.random()*17+20
                }
            }
        },
        State {
            name: "online_prepare"
            PropertyChanges {
                target: timer
                length: 2
                onFinished: function (){
                    page.state = "prepare"
                    notification.createObject(window,
                                              {titleText: "",
                                               displayText: qsTr("GO!!!"),
                                               displayTime: 950})
                }
                onTick: function (){
                    notification.createObject(window,
                                              {titleText: "",
                                               displayText: timer.length === 2 ? "Ready?" : "Steady!",
                                               displayTime: 950})
                }
            }
            PropertyChanges {
                target: elephant
                preparing: false
            }
            StateChangeScript {
                script: timer.start()
            }
        },
        State {
            name: "prepare"
            PropertyChanges {
                target: timer
                length: preparationTime
                onFinished: function (){
                    page.state = "position"
                    notification.createObject(window,
                                              {titleText: qsTr("Time to"),
                                               displayText: qsTr("FIGHT!!!"),
                                               displayTime: 2000})
                }
                onTick: function (){
                    notification.createObject(window,
                                              {titleText: qsTr("Time left:"),
                                               displayText: timer.length,
                                               displayTime: 950})
                }
            }
            PropertyChanges {
                target: elephant
                preparing: true
            }
            StateChangeScript {
                script: timer.restart()
            }
        },
        State {
            name: "position"
            PropertyChanges {
                target: timer
                onTick: function() {}
                onFinished: function (){
                    page.state = "fight"
                }
                length: 0
            }
            AnchorChanges {
                target: ennemy
                anchors.left: parent.horizontalCenter
            }
            AnchorChanges {
                target: elephant
                anchors.right: parent.horizontalCenter
            }
            StateChangeScript {
                script: {
                    if (page.onlinePlay) {
                        gameClient.finish(elephant.xp)
                    }
                }
            }
            StateChangeScript {
                script: timer.restart()
            }
        },
        State {
            name: "fight"
            PropertyChanges {
                target: timer
                interval: 100
                length: 50
                onTick: function () {
                    elephant.fire()
                    ennemy.fire()
                }
                onFinished: function () {
                    page.state = "finished"
                }
            }
            AnchorChanges {
                target: ennemy
                anchors.left: parent.horizontalCenter
            }
            AnchorChanges {
                target: elephant
                anchors.right: parent.horizontalCenter
            }
            StateChangeScript {
                script: timer.restart()
            }
        },
        State {
            name: "finished"
            AnchorChanges {
                target: elephant
                anchors.right: won ? parent.right : parent.left
            }
            AnchorChanges {
                target: ennemy
                anchors.left: won ? parent.right : parent.left
            }
            StateChangeScript {
                script: {
                    timer.stop()

                    gameClient.signOut()

                    endDialog.open()
                    if (won) {
                        if (onlinePlay) {
                            onlinePlay = false
                            settings.wonOnline = settings.wonOnline + 1
                        } else {
                            settings.wonOffline = settings.wonOffline + 1
                        }


                        ennemy.explode()

                        if (settings.lootRate > Math.random()) {
                            var unlocked
                            if (settings.directedLootRate > Math.random()) {
                                var headLimit = elephant.parts[Sets.Head].xp/elephant.xp
                                var earsLimit = elephant.parts[Sets.Ears].xp/elephant.xp + headLimit
                                var bodyLimit = elephant.parts[Sets.Body].xp/elephant.xp + earsLimit
                                var feetLimit = 1
                                var rand = Math.random()
                                unlocked = (rand > bodyLimit) + (rand > feetLimit) + (rand > earsLimit) + (rand > headLimit)
                            } else {
                                unlocked = Math.floor(Math.random()*4)
                            }

                            sets.unlock(unlocked, ennemy.parts[unlocked].set)
                        }
                    } else {
                        elephant.explode()
                        settings.lost++
                    }
                }
            }
        }
    ]
}
