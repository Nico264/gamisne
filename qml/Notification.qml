import QtQuick 2.10
import QtQuick.Controls 2.3

Component {
    Item {
        id: notification
        property string titleText
        property string displayText
        property int displayTime: 4000
        height: display.height+title.height
        width: window.width
        property int desiredY: window.height-tabBar.height-(openedNotifications.indexOf(this)+1)*height

        YAnimator on y {
            id: animator
            from: y ? y : desiredY
            to: desiredY
            onToChanged: restart()
            duration: 500
        }

        Component.onCompleted: {
            openedNotifications.push(this)
            openedNotifications = openedNotifications
        }
        Component.onDestruction: {
            var index = openedNotifications.indexOf(this)
            openedNotifications.splice(index, 1)
            openedNotifications = openedNotifications
        }

        Timer {
            interval: displayTime
            running: true
            onTriggered: parent.destroy()
        }

        Label {
            id: title
            text: titleText
            color: "white"
            font.pixelSize: Qt.application.font.pixelSize * 2
            SequentialAnimation on x {
                NumberAnimation {
                    from: notification.width
                    to: -title.contentWidth
                    duration: displayTime
                    easing.type: Easing.OutInBack
                }
            }
        }

        Label {
            id: display
            anchors.top: title.bottom
            text: displayText
            color: "white"
            font.pixelSize: Qt.application.font.pixelSize * 3
            SequentialAnimation on x {
                NumberAnimation {
                    from: -display.contentWidth
                    to: notification.width
                    duration: displayTime
                    easing.type: Easing.OutInBack
                }
            }
        }
    }
}
