import QtQuick 2.10
import QtQuick.Controls 2.3

Item {
    id: tutorial

    property bool inProgress: false

    property var nextStep: function () {}

    function start(){
        inProgress = true
        tabBar.enabled = false
        dialog.title = "Bienvenue dans GAMISNE"
        label.text = "Il semblerait que ce soit votre première partie.\n"+
                "Commencez par marteler cet œuf SVG."
        nextStep = function(){}
        dialog.open()
    }

    function egg_blown(){
        dialog.title = "Bien joué !"
        label.text = "Mais WTF ? C'est un éléphant ça ! :-O\n"+
                "Depuis quand les éléphants ça pousse dans des œufs ?\n"
        nextStep = function(){suspens()}
        dialog.open()
    }

    function suspens(){
        dialog.title = "La suite ?"
        label.text = "Cliquez le plus de fois possible sur votre éléphant dans le temps imparti…"
        nextStep = function(){page.state = "online_prepare"}
        dialog.open()
    }

    function explain(){
        settings.tutorialSeen = true
        dialog.title = "Vous êtes prêts"
        label.text = "Ça y est, vous avez les bases, mais voici l'objectif du jeu :\n"+
                "Notre team graphisme a dessiné plusieurs modèles d'éléphants, l'objectif est de tous les collectionner. "+
                "Lorsque vous remportez une victoire, vous gagnez une partie de votre adversaire."
        nextStep = function(){explain_buttons()}
        dialog.open()
    }

    function explain_buttons(){
        inProgress = false
        dialog.title = "Les bouttons"
        label.text = "En haut de votre écran, deux boutons viennent d'apparaître.\n"+
                " - Le premier vous permet de jouer en ligne et donc de défier vos amis\n"+
                " - Le second vous permet de modifier votre éléphant\n"

        nextStep = function(){explain_pages()}
        dialog.open()
    }

    function explain_pages(){
        tabBar.enabled = true
        dialog.title = "Les pages"
        label.text = "En bas de votre écran, la navigation est activée, avec\n"+
                " - Cette page, pour le combat\n"+
                " - Une page pour améliorer vos drops\n"+
                " - Une page qui regroupe vos trophées\n"+
                "\n"+
                "Bon jeu !"

        nextStep = function(){settings.tutorialSeen = true}
        dialog.open()
    }

    Dialog{
        id: dialog

//        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)
        width: Math.min(Math.max(header.implicitWidth, dummyLabel.implicitWidth + 2*padding), 5/6*page.width)
        leftMargin: (page.width-width)/2
        modal: true
        focus: true
        dim: false
        closePolicy: Popup.CloseOnEscape

        standardButtons: Dialog.Ok

        onClosed: nextStep()

        Label {
            id: label
            wrapMode: Text.WordWrap
            anchors.fill: parent

            Label {
                id: dummyLabel
                text: label.text
                visible: false
            }
        }
    }
}
