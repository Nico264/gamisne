import QtQuick 2.10
import Qt.labs.settings 1.0

Item {
    id: upgrades

    Component.onCompleted: {
        list.get(Upgrades.MoreParts).number = Math.round((settings.lootRate - 0.3)/0.05)
        list.get(Upgrades.MoreChoice).number = Math.round(settings.directedLootRate/0.05)
    }

    function unlock(upgrade){
        notification.createObject(window,
                                  {titleText: qsTr("Potion achetée !"),
                                   displayText: list.get(upgrade).name})
        settings.currentClicks -= list.get(upgrade).price
        list.get(upgrade).number++
        switch (upgrade) {
        case Upgrades.MoreParts:
            settings.lootRate += 0.05
            break;
        case Upgrades.MoreChoice:
            settings.directedLootRate += 0.05
            break;
        }
    }

    enum Upgrades{
        MoreParts,
        MoreChoice
    }

    property ListModel list: ListModel {
/*
        ListElement {
            name: ""
            comment: ""
            prince: 0
            unlocked: false
        }
*/

        ListElement {
            name: "Confetisator"
            comment: "Augmente vos chances de remporter des pièces."
            price: 100
            number: 0
        }

        ListElement {
            name: "Stupefix !"
            comment: "Augmente vos chance de remporter la pièce sur laquelle vous avez le plus cliqué."
            price: 200
            number: 0
        }
    }
}
