import QtQuick 2.10
import Qt.labs.settings 1.0

Item {
    id: sets
    signal newPart(int partType, int set)

    function unlock(part, set) {
        if (unlocked[part].indexOf(set) === -1) {
            unlocked[part].push(set)
            unlocked = unlocked
            newPart(part, set)

            if (unlocked[part].length === setsList.count) {
                achievements.unlock(Achievements.AllParts+1+part)
                var all = true
                unlocked.forEach(function(list){
                    all = all && (list.length === setsList.count)
                })
                if (all){
                    achievements.unlock(Achievements.AllParts)
                }
            }
        }
    }

    function randomSet() {
        return [
                    [Math.floor(Math.random() * setsList.count)],
                    [Math.floor(Math.random() * setsList.count)],
                    [Math.floor(Math.random() * setsList.count)],
                    [Math.floor(Math.random() * setsList.count)]
                ]
    }

    function getSetName(set) {
        return setsList.get(set).name
    }

    function getPartName(partType) {
        return partTypesList[partType]
    }

    function getStartPoint(set) {
        return Qt.point(setsList.get(set).startX, setsList.get(set).startY)
    }

    property var unlocked: [[-1], [-2], [-3], [-4]]

    Settings {
        id: save
        category: "sets"

        property alias unlocked: sets.unlocked
        property bool init: true
        Component.onCompleted: {
            if (init) {
                unlocked = randomSet()
                init = false
            }
        }
    }

    enum Parts {
        Head,
        Ears,
        Body,
        Feet
    }

    enum Sets {
        Abso,
        Arsn,
        Atlas,
        Bluff,
        Chtulhu,
        Clint,
        Gaming,
        Ghib,
        HP,
        Jack,
        Jones,
        Lord,
        Macotte,
        Teletub,
        Wizard
    }

    property var partTypesList: ["head", "ears", "body", "feet"]

    property ListModel setsList: ListModel{
        ListElement{
            name: "abso"
            startX: 566 // TODO
            startY: 760 // TODO
        }
        ListElement{
            name: "arsn"
            startX: 566 // TODO
            startY: 760 // TODO
        }
        ListElement{
            name: "atlas"
            startX: 566 // TODO
            startY: 760 // TODO
        }
        ListElement{
            name: "bluff"
            startX: 566
            startY: 760
        }
        ListElement{
            name: "chtulhu"
            startX: 492
            startY: 690
        }
        ListElement{
            name: "clint"
            startX: 566
            startY: 760
        }
        ListElement{
            name: "gaming"
            startX: 566
            startY: 760
        }
        ListElement{
            name: "ghib"
            startX: 566
            startY: 760
        }
        ListElement{
            name: "hp"
            startX: 566
            startY: 760
        }
        ListElement{
            name: "jack"
            startX: 566
            startY: 760
        }
        ListElement{
            name: "jones"
            startX: 566 // TODO
            startY: 760 // TODO
        }
        ListElement{
            name: "lord"
            startX: 614
            startY: 908
        }
        ListElement{
            name: "mascotte"
            startX: 566 // TODO
            startY: 760 // TODO
        }
        ListElement{
            name: "teletub"
            startX: 374
            startY: 166
        }
        ListElement{
            name: "wizard"
            startX: 684
            startY: 490
        }
    }
}
