import QtQuick 2.10
import QtQuick.Controls 2.3

Dialog {
    title: "À propos de nous"

    //        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)
    width: Math.min(Math.max(header.implicitWidth, dummyLabel.implicitWidth + 2*padding), 5/6*page.width)
    topMargin: (page.height-height)/2
    leftMargin: (page.width-width)/2
    modal: true
    focus: true
    dim: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    standardButtons: Dialog.Ok

    Label {
        id: label
        wrapMode: Text.WordWrap
        anchors.fill: parent
        text: "Nous somme la Wiz'Art, une des deux listes BDA de l'année scolaire 2017/2018 de l'ENSIMAG.\n"+
              "Nous vous proposons ce petit jeu dans le cadre de nos campagnes sans aucune garantie qu'il fonctionne ni qu'il soit juste.\n"+
              "\n"+
              "Cette application utilise le framework Qt5 sous licence GPL. Les sources sont disponibles sur https://gitlab.com/Nico264/gamisne."

        Label {
            id: dummyLabel
            text: label.text
            visible: false
        }
    }

}
