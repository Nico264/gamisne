import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0

Page {
    id: page

    background: Image {
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/images/achievements/background.png"
    }

    header: Label {
        text: "Succés"
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
        width: parent.width
        background: Image {
            source: "qrc:/images/achievements/top.png"
        }
    }

    GridView {
        id: gridView
        anchors.fill: parent
        model: achievements.list

        anchors.leftMargin: 70/930*parent.width
        anchors.rightMargin: 70/930*parent.width

        displayMarginBeginning: page.header.height

        cellWidth: width/Math.floor(width/100)
        cellHeight: 160

        delegate: Item {
            width: gridView.cellWidth
            height: gridView.cellHeight
            MouseArea {
                onClicked: {
                    if (unlocked) {
                        dialog.title = model.name
                        dialog.comment = model.comment
                        dialog.open()
                    }
                }
                anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Image {
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 35
                anchors.horizontalCenter: parent.horizontalCenter
                id: trophy
                source: "qrc:/images/achievements/trophy.png"
                sourceSize.width: 100
            }

            Desaturate {
                anchors.fill: trophy
                source: trophy
                desaturation: unlocked ? 0 : 1
            }

            Label {
                anchors.bottom: trophy.bottom
                text: unlocked ? name : "???"
                anchors.horizontalCenter: parent.horizontalCenter
                bottomPadding: 10
                color: "purple"
            }

            Image {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: -gridView.anchors.leftMargin+27/930*page.width
                source: "qrc:/images/achievements/cupboard.png"
                width: (930-46)/930*page.width
                height: header.height
                visible: index % Math.floor(gridView.width/gridView.cellWidth)=== 0
                z: -1
            }
        }
    }

    Dialog {
        id: dialog
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        property string comment
        Label {
            text: dialog.comment
            wrapMode: Text.WordWrap
            anchors.fill: parent
        }
        width: Math.min(Math.max(header.implicitWidth, implicitWidth), 5/6*page.width)
        topMargin: (page.height-height)/2
        leftMargin: (page.width-width)/2
        modal: true
    }
}
