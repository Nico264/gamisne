import QtQuick 2.10
import Qt.labs.settings 1.0

Item {
    id: achievements

    function unlock(achievement){
        if (settings.loaded && !list.get(achievement).unlocked) {
            unlocked.push(achievement)
            unlocked = unlocked
            notification.createObject(window,
                                      {titleText: qsTr("Succés débloqué!"),
                                       displayText: list.get(achievement).name})
            list.get(achievement).unlocked = true
        }
    }

    property var unlocked: []

    Settings {
        category: "achievements"

        property alias unlocked: achievements.unlocked
        property bool loaded: false

        Component.onCompleted: {
            unlocked.forEach(function(achievement, index){
                unlocked[index] = Number(achievement)
                list.get(unlocked[index]).unlocked = true
            })
            unlocked = unlocked
            loaded = true
        }
    }

    enum Achievements{
        Hatched,
        FirstBot,
        FifthBot,
        TenthBot,
        TwentiethBot,
        FiftiethBot,
        HundredthBot,
        FirstBlood,
        FifthBlood,
        TenthBlood,
        TwentiethBlood,
        FiftiethBlood,
        HundredthBlood,
        CompleteSet,
        AbsoSet,
        ArsnSet,
        AtlasSet,
        BluffSet,
        ChtulhuSet,
        ClintSet,
        GamingSet,
        GhibSet,
        HPSet,
        JackSet,
        JonesSet,
        LordSet,
        MascotteSet,
        TeletubSet,
        WizardSet,
        AllParts,
        HeadParts,
        EarsParts,
        BodyParts,
        FeetParts
    }

    property ListModel list: ListModel {
/*
        ListElement {
            name: ""
            comment: ""
            unlocked: false
        }
*/

        ListElement {
            name: "Éclos"
            comment: "Vous avez ouvert votre premier œuf !"
            unlocked: false
        }

        ListElement {
            name: "Bot killer"
            comment: "You killed an elephant, but there is no blood… Seems like it was a bot…"
            unlocked: false
        }

        ListElement {
            name: "Tout seul !"
            comment: "Il y a personne sur le serveur ? 5 bots battus"
            unlocked: false
        }

        ListElement {
            name: "Décimés"
            comment: "Vous êtes monstrueux :-\ 10 bots battus"
            unlocked: false
        }

        ListElement {
            name: "Asocial"
            comment: "Trouvez-vous des amis pour jouer… 20 bots battus"
            unlocked: false
        }

        ListElement {
            name: "Still here"
            comment: "Vous avez compris qu'on pouvait jouer en multi, nan ? 50 bots battus"
            unlocked: false
        }

        ListElement {
            name: "Hécatombe"
            comment: "On pensait pas que vous resteriez aussi longtemps… 100 bots battus"
            unlocked: false
        }

        ListElement {
            name: "First blood"
            comment: "Premier combat en ligne remporté"
            unlocked: false
        }

        ListElement {
            name: "Bon départ"
            comment: "5 combats en ligne remportés, ça se fête !"
            unlocked: false
        }

        ListElement {
            name: "Mayhem"
            comment: "Whaou ! 10 combats en ligne remportés !"
            unlocked: false
        }

        ListElement {
            name: "Compèt'"
            comment: "Vous êtes prêt pour le niveau national, 20 victoires en ligne !"
            unlocked: false
        }

        ListElement {
            name: "Smashed"
            comment: "Vous avez l'air meilleur qu'à Smash Bros, avec 50 victoires online."
            unlocked: false
        }

        ListElement {
            name: "Surpuissant"
            comment: "On risque de vous demander pourquoi vos doigts sont si musclés… 100 victoire en ligne !"
            unlocked: false
        }

        ListElement {
            name: "Pokémon !"
            comment: "Attrapez-les tous!"
            unlocked: false
        }

        ListElement {
            name: "Votre majestée !"
            comment: "Vous avez retrouvé le roi !"
            unlocked: false
        }

        ListElement {
            name: "Habile !"
            comment: "Nous allons voler… l'ENSIMAG !"
            unlocked: false
        }

        ListElement {
            name: "Atlast"
            comment: "La terre repose sur vous"
            unlocked: false
        }

        ListElement {
            name: "Poker !"
            comment: "Je suis bluffé !"
            unlocked: false
        }

        ListElement {
            name: "Set Chtulhu"
            comment: "Qu'est-ce qu'il fout ici ?¿?"
            unlocked: false
        }

        ListElement {
            name: "Listwood"
            comment: "#VraiCampagne"
            unlocked: false
        }

        ListElement {
            name: "Gaming"
            comment: "À quand le multi d'ENSIMAGar.io ?"
            unlocked: false
        }

        ListElement {
            name: "Ghib"
            comment: "#PrécédenteVraiCampagne"
            unlocked: false
        }

        ListElement {
            name: "Harry Potter"
            comment: "Mais puisqu'on vous dit que c'est pas notre thème !"
            unlocked: false
        }

        ListElement {
            name: "Jack"
            comment: "C'est plus une sorte de guide"
            unlocked: false
        }

        ListElement {
            name: "Coup de fouet"
            comment: "D’aucun ont des aventures, nous SOMMES une aventure"
            unlocked: false
        }

        ListElement {
            name: "Set Lord"
            comment: "Il reignera en maître sur le BDE"
            unlocked: false
        }

        ListElement {
            name: "Mascotte IMAG 2017"
            comment: "C'est pas un tutu"
            unlocked: false
        }

        ListElement {
            name: "Teletublist"
            comment: "Noo-Noo ne vous sauvera pas toujours"
            unlocked: false
        }

        ListElement {
            name: "C'est nous"
            comment: "Bienvenue dans la li…iste !"
            unlocked: false
        }

        ListElement {
            name: "Au complet"
            comment: "Vous avez plus ou moins terminé le jeu : vous avez tous les sets"
            unlocked: false
        }

        ListElement {
            name: "Tête à tête"
            comment: "Vous faites touner les têtes !"
            unlocked: false
        }

        ListElement {
            name: "Omniscient"
            comment: "What's that noise?"
            unlocked: false
        }

        ListElement {
            name: "Surchage"
            comment: "Tellement de corps d'éléphants dans ce sac…"
            unlocked: false
        }

        ListElement {
            name: "C'est le pied"
            comment: "Ça me fait de belles jambes"
            unlocked: false
        }
    }
}
